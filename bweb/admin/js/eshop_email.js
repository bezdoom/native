function updateMail(id) {
	
	$('#al'+id).html('<img src="/bweb/admin/img/ajax-loader.gif" />');
	
	var titlename = document.getElementById('MAIL_item' + id).value;

	$.ajax({
	  url: "/bweb/admin/ajax/eshop_email.php?update",
	  type: "POST",
	  dataType: "json",
	  data: 'id=' + escape(id) + '&MAIL_item' + escape(id) + '=' + escape(titlename) ,
	
	  success: function(res) {
	  	if (res.result == 1) {
	  		$('#MAIL_item'+id).css('border','1px solid #2C8C11');
	  	}else{
	  		$('#MAIL_item'+id).css('border','1px solid #E00B0B');
	  	}
	  	$('#al'+id).html(id);
	    return false;
	  },
	
	});
	
	return false;

}


function deleteMail(id) {
	$('#al'+id).html('<img src="/bweb/admin/img/ajax-loader.gif" />');
	
	$.ajax({
	  url: "/bweb/admin/ajax/eshop_email.php?delete",
	  type: "POST",
	  dataType: "json",
	  data: 'id=' + escape(id) ,
	
	  success: function(res) {
	  	if (res.result == 1) {
	  		$('#tr'+id).remove();
	  	}else{
	  		$('#MAIL_item'+id).css('border','1px solid #E00B0B');
	  		$('#al'+id).html(id);
	  	}
	    return false;
	  },
	
	});
	
	return false;
	
}

function addMail(id) {
	$('#tradd').html('<img src="/bweb/admin/img/ajax-loader.gif" />');
	
	var titlename = document.getElementById('MAIL_item').value;
	
	$.ajax({
	  url: "/bweb/admin/ajax/eshop_email.php?add",
	  type: "POST",
	  dataType: "json",
	  data: '&MAIL_item=' + escape(titlename) ,
	
	  success: function(res) {
	  	if (res.result == 1) {
	  		$('#appendtd').append(res.htm);
	  	}else{
	  		$('#MAIL_item').css('border','1px solid #E00B0B').delay(400).css('border','');
	  	}
	  	document.getElementById('MAIL_item').value = '';
	  	$('#tradd').html('ADD');
	    return false;
	  },
	
	});
	
	return false;
	
}