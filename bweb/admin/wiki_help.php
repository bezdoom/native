<?
// ���������� ����
require '../../bweb/system.php';
// ��������� ������ �������

// ��������� ����
include_once('.menu.php');

$GLOBALS['NATIVE_ADMIN']['TITLE_ADM'] = '���������� �����';
include_once('menu/native.menu.php');

//env::debugmode(); // ���������� ���������� ���������� - ���������������� ����� �� ����������
// �������� ��������������� ���� ��������
ob_start();
?>	
		
<?if(USER::IsAuthorized()) {?>
<?if(USER::IsAdmin()) {?>

<h2>���������� ����� :: Wiki-��������</h2>

<div>
<div>
<br>
<p>��� ���������� ����� ������� �� ����� ����� ������������ ���������������� ��������!
����� �� ������ ������������ �� ����� ���������� �� ������ ������ ���������� ��������!</p>

<div style="border-bottom:1px solid #ccc; padding:7px; margin-bottom:3px;">

<h4 style="margin-top:0px; margin-bottom:6px;"> ������� ������ </h4>

<table style="width:965px;">

<tr>

<td style="padding-right:8px;" width="20%">
<center>
    <code style="border:1px dashed #E2E1DE; padding:4px; background-color:#F7F7F7">
    {br}
    </code>
    </center>
    </td>
    <td style="padding-left:8px; border-left:1px dashed #ccc" width="80%">
     ������� ������! ��������� � ��� �����, ��� ��������� �������!  </td>
</tr>
</table>
</div>
<div style="border-bottom:1px solid #ccc; padding:7px; margin-bottom:3px;">
<h4 style="margin-top:0px; margin-bottom:6px;"> ������ ������� </h4>
<table style="width:965px;">
<tr>
<td style="padding-right:8px;" width="20%">
    <center>
    <code style="border:1px dashed #E2E1DE; padding:4px; background-color:#F7F7F7">
    {both}
    </code>
    </center>
    </td>
    <td style="padding-left:8px; border-left:1px dashed #ccc" width="80%">
     ������������� ������ ������� ��� ��������� ������!  </td>
</tr>
</table>
</div>
<h3><p>�������</p></h3>
<div style="border-bottom:1px solid #ccc; padding:7px; margin-bottom:3px;">
<h4 style="margin-top:0px; margin-bottom:6px;"> ������������ ������� </h4>
<table>
<tr>
<td style="padding-right:8px;" width="50%">
    <pre style="border:1px dashed #E2E1DE; padding:4px; background-color:#F7F7F7"><div class="font_table">
{||

{! �������� ������� !}

!!--

 [!��������� 1!] [!��������� 2!] [!��������� 3!]

--!!

!!--

 [|������ 1|] [|������ 2|] [|������ 3|]

--!!

!!--

 [|������ 4|] [|������ 5|] [|������ 6|]

--!!

||}
</div>
    </pre>
    </td>
    <td style="padding-left:8px; border-left:1px dashed #ccc" width="50%">
    ��������! ���������� ������� ������ ���������� � ����� ������ �� ���� ��� �������!
     ����� ����������� ������ ������� ��� ����� ��������! <br />
     ��� ���� ������� �������� ����� ������ <b>{|| ���� ������� ||}</b><br />
     ��������� ���� <b>{! �������� !}</b> �� ���������� - ��������� �������� ��� �������<br />
     ���� <b>!!-- ������ --!!</b> ���������� ������ �������. ������� ����������� ���������� �����!<br />
     ������ <b>[! �������� ������� !]</b> - ���������� �������� �������! �� �����������! ������� ����������� ���������� ������� �� ������! <br />
     ������ <b>[| ������� |]</b> - ���������� �������� � ������! ������� ����������� ���������� ������� �� ������! <br />
     ������ �����, ��� � ������ �������� ������ !!-- --!! ������ ���� ���������� ���������� ������ [! !] � [| |]. � ���� ������� ����� ������������ ����������� ����� ��� wiki!
     </td>
</tr>
</table>
</div>
<div style="border-bottom:1px solid #ccc; padding:7px; margin-bottom:3px;">
<h4 style="margin-top:0px; margin-bottom:6px;"> ���������� ������ ������� </h4>
<table>
<tr>
<td style="padding-right:8px;" width="50%">
<pre style="border:1px dashed #E2E1DE; padding:4px; background-color:#F7F7F7"><div class="font_table">
{noborder||

{! �������� ������� !}

!!--

 [!��������� 1!] [!��������� 2!] [!��������� 3!]

--!!

!!--

 [|������ 1|] [|������ 2|] [|������ 3|]

--!!

!!--

 [|������ 4|] [|������ 5|] [|������ 6|]

--!!

||}
</div>
    </pre>

    </td>

    <td style="padding-left:8px; border-left:1px dashed #ccc" width="50%">

     ���������� ������������ �������! ������������, ��� ����� - ������� �������� noborder: <b>{noborder|| ���� ������� ||}</b>

     </td>

</tr>

</table>

</div>



<h3><p>������ ���� � �������� ��� ����������</p></h3>



<div style="border-bottom:1px solid #ccc; padding:7px; margin-bottom:3px;">

<h4 style="margin-top:0px; margin-bottom:6px;"> �������� </h4>

<table style="width:965px;">

<tr>

	<td style="padding-right:8px;" width="20%">

    <center>

    <code style="border:1px dashed #E2E1DE; padding:4px; background-color:#F7F7F7">

    ``` ����� ```

    </code>

    </center>

    </td>

    <td style="padding-left:8px; border-left:1px dashed #ccc" width="80%">

     ����������� ��������! ���������� � HTML: 

    &lt;p&gt;test&lt;/p&gt;. </td>

</tr>

</table>

</div>



<div style="border-bottom:1px solid #ccc; padding:7px; margin-bottom:3px;">

<h4 style="margin-top:0px; margin-bottom:6px;"> �������� ���������� </h4>

<table style="width:965px;">

<tr>

	<td style="padding-right:8px;" width="20%">

    <center>

    <code style="border:1px dashed #E2E1DE; padding:4px; background-color:#F7F7F7">

    ```` ����� ````

    </code>

    </center>

    </td>

    <td style="padding-left:8px; border-left:1px dashed #ccc" width="80%">

     ��������, ������������� ���� �� ����� �������� �� ���� ������! <br>���������� � HTML: 

    &lt;p align="fustify"&gt;test&lt;/p&gt;. </td>

</tr>

</table>

</div>



<div style="border-bottom:1px solid #ccc; padding:7px; margin-bottom:3px;">

<h4 style="margin-top:0px; margin-bottom:6px;"> ��������� �������� </h4>

<table style="width:965px;">

<tr>

	<td style="padding-right:8px;" width="20%">

    <center>

    <code style="border:1px dashed #E2E1DE; padding:4px; background-color:#F7F7F7">

    '' ����� ''

    </code>

    </center>

    </td>

    <td style="padding-left:8px; border-left:1px dashed #ccc" width="80%">

     �������� ����� ��������! ���������� � HTML: 

    &lt;i&gt;test&lt;/i&gt;. </td>

</tr>

</table>

</div>



<div style="border-bottom:1px solid #ccc; padding:7px; margin-bottom:3px;">

<h4 style="margin-top:0px; margin-bottom:6px;"> ������������ �� ������ </h4>

<table style="width:965px;">

<tr>

	<td style="padding-right:8px;" width="20%">

    <center>

    <code style="border:1px dashed #E2E1DE; padding:4px; background-color:#F7F7F7">

    {cen} ����� {/cen}

    </code>

    </center>

    </td>

    <td style="padding-left:8px; border-left:1px dashed #ccc" width="80%">

     ������������ �� ������. </td>

</tr>

</table>

</div>



<div style="border-bottom:1px solid #ccc; padding:7px; margin-bottom:3px;">

<h4 style="margin-top:0px; margin-bottom:6px;"> ��������� ������ </h4>

<table style="width:965px;">

<tr>

	<td style="padding-right:8px;" width="20%">

    <center>

    <code style="border:1px dashed #E2E1DE; padding:4px; background-color:#F7F7F7">

    ''' ����� '''

    </code>

    </center>

    </td>

    <td style="padding-left:8px; border-left:1px dashed #ccc" width="80%">

     �������� ����� ������! ���������� � HTML: 

    &lt;b&gt;test&lt;/b&gt;. </td>

</tr>

</table>

</div>



<div style="border-bottom:1px solid #ccc; padding:7px; margin-bottom:3px;">

<h4 style="margin-top:0px; margin-bottom:6px;"> ����������� �����! </h4>

<table style="width:965px;">

<tr>

	<td style="padding-right:8px;" width="20%">

    <center>

    <code style="border:1px dashed #E2E1DE; padding:4px; background-color:#F7F7F7">

    '''' <s>�����</s> ''''

    </code>

    </center>

    </td>

    <td style="padding-left:8px; border-left:1px dashed #ccc" width="80%">

     ����������� �����! ���������� � HTML: 

    &lt;s&gt;test&lt;/s&gt;. </td>

</tr>

</table>

</div>



<div style="border-bottom:1px solid #ccc; padding:7px; margin-bottom:3px;">

<h4 style="margin-top:0px; margin-bottom:6px;"> ��������� 1 </h4>

<table style="width:965px;">

<tr>

	<td style="padding-right:8px;" width="20%">

    <center>

    <code style="border:1px dashed #E2E1DE; padding:4px; background-color:#F7F7F7">

    ===<u>�����</u>===

    </code>

    </center>

    </td>

    <td style="padding-left:8px; border-left:1px dashed #ccc" width="80%">

     �������� ��������� � ��������������! </td>

</tr>

</table>

</div>



<div style="border-bottom:1px solid #ccc; padding:7px; margin-bottom:3px;">

<h4 style="margin-top:0px; margin-bottom:6px;"> ��������� 2 </h4>

<table style="width:965px;">

<tr>

	<td style="padding-right:8px;" width="20%">

    <center>

    <code style="border:1px dashed #E2E1DE; padding:4px; background-color:#F7F7F7">

    == ����� ==

    </code>

    </center>

    </td>

    <td style="padding-left:8px; border-left:1px dashed #ccc" width="80%">

     �������������� ���������! </td>

</tr>

</table>

</div>



<div style="border-bottom:1px solid #ccc; padding:7px; margin-bottom:3px;">

<h4 style="margin-top:0px; margin-bottom:6px;"> ������ ��� �������� </h4>

<table>

<tr>

	<td style="padding-right:8px;" width="30%">

    <center>

    <code style="border:1px dashed #E2E1DE; padding:4px; background-color:#F7F7F7">

    [[http://www.taldom.org]]

    </code>

    </center>

    </td>

    <td style="padding-left:8px; border-left:1px dashed #ccc" width="70%">

     ������� ������, � ���� ������ � ��������, ������� ��������� ����� ������� ���������� ������ </td>

</tr>

</table>

</div>



<div style="border-bottom:1px solid #ccc; padding:7px; margin-bottom:3px;">

<h4 style="margin-top:0px; margin-bottom:6px;"> ������ � ��������� </h4>

<table>

<tr>

	<td style="padding-right:8px;" width="40%">

    <center>

    <code style="border:1px dashed #E2E1DE; padding:4px; background-color:#F7F7F7">

    [[http://www.taldom.org][������-������]]

    </code>

    </center>

    </td>

    <td style="padding-left:8px; border-left:1px dashed #ccc" width="60%">

     ������� ������, � ���� ������ ����� ������ ����� ���������� ������ � ��������� ����� ������ ����� </td>

</tr>

</table>

</div>



<div style="border-bottom:1px solid #ccc; padding:7px; margin-bottom:3px;">

<h4 style="margin-top:0px; margin-bottom:6px;"> ������������ ������ </h4>

<table style="width:965px;">

<tr>

	<td style="padding-right:8px;" width="30%">

    

    <pre style="border:1px dashed #E2E1DE; padding:4px; background-color:#F7F7F7"><div class="font_table">

{*

   * ����� 1

   * ����� 2

*}</div></pre>

   

    </td>

    <td style="padding-left:8px; border-left:1px dashed #ccc" width="70%">

     ������� ������������ ������, ��������� � �������� ������� ��������, <br>������������ �� ����� *. ���� ����� - ���� ������! </td>

</tr>

</table>

</div>



<div style="border-bottom:1px solid #ccc; padding:7px; margin-bottom:3px;">

<h4 style="margin-top:0px; margin-bottom:6px;"> ����������� ������ </h4>

<table style="width:965px;">

<tr>

	<td style="padding-right:8px;" width="30%">

    

    <pre style="border:1px dashed #E2E1DE; padding:4px; background-color:#F7F7F7"><div class="font_table">

{# 

   # ����� 1

   # ����� 2

#}</div></pre>

    

    </td>

    <td style="padding-left:8px; border-left:1px dashed #ccc" width="70%">

     ���������� ������������� ������! </td>

</tr>

</table>

</div>



<h3><p>������ ��������: </p></h3>

<pre><div class="font_size">

=== ������ ===

``` ������ � ������� ���������� ���������, ����� ������������ ��������, ���������������� ����� 

������������ ������������ ������ � ���������� �������, � ������ ������� �� ������. ����� ���� 

����������, ���������, ������ ��������������� ������ � �������� ��������������, ����, ���� � ����.

�����-����� (1965), ��������� ����� �������� ������ (1947, 1965) � ������� ����������� ��������� (1967). ```



``` ������ � ��������� ������������ ����, � ����� ������������, �������������, ���������� � ������� ����� 

������. ����� ����������� 6 ����������, 9 ��������������� ��������, 3 ������ ����� (������� ������ ��������� 

� ������ ��������� �������������� � ��������� ���������� �������). � 1935 ���� � ������� �������� ������������. ```



== ������: ���������������-��������������� ������� ==

```

{*

* ����������, ����������� � ������� �������� ������ ������ ����;

* ���������� � �������� ���������� ��������� ������������� ������ (����);

*}

```
</div>
</pre> 

</div>
<?if ($_GET['f']!=false) {
	
	$selfile = ROOTDIR . DS . 'bweb' . DS . 'areas' . DS . $_GET['f'];
	
	if (file_exists($selfile)) {
		
		if (isset($_GET['save']) && isset($_POST['content'])) {
			$file2save = fopen($selfile, 'w+');
			$ifok = fwrite($file2save, stripslashes($_POST['content'])); // ������ � ����
			fclose($file2save);
			if ($ifok) {
				Header('Location: /bweb/admin/native_areas.php');
				exit;
			}else{
				$errmess = "������ �� ���� ���������!";
			}
		}
		
		ob_start();
		readfile($selfile); // ��������� �����	
		$filecontent = ob_get_contents();
		ob_end_clean();
	}else{
		$errmess = "��������� ���� �� ������ �� �������!";
	}
?>	

<?if ($errmess!=false) { ?>
	<?=$errmess?>
<?}else{?>
<form action="/bweb/admin/native_areas_edit.php?save&f=<?=$_GET['f']?>" method="POST">	
��� �������� ������ ����������� wiki-�������� <a href="bweb/wiki_help.html" target="_blank">[������� ��������� ��� ��������]</a></br><a style="float:right" href="/bweb/admin/native_areas.php">[�����]</a><br />
<textarea style="width:700px; height:400px" name="content"><?=$filecontent?></textarea><br />
<input type="submit" value="��������� ���������" />
</form>	
<?}?>	
<?}?>


</div>

		
<?}else{?>
		<?
		USER::logout();
		header('Location: /bweb/admin/');
		?>
	<?}?>
<?}else{?>
	<?components::area('admin/authform.php')?>
<?}?>

<?		
// �������� ����� �������� � ����������
$obresult = ob_get_contents();
ob_end_clean();

/////////////////////////////////////////////////////////////////////////////////
///////////	���������� ������ � �������� ////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
	
native::header('cp_new', array(
		'title' => '������ ����������',
	));
	
print $obresult;

native::footer();						
				
?>