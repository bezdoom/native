<?

// ��������� ��� mysql
include_once('db_conf.php');

// ��������������� ���������� ��� �������� ��������� �������
include_once('prepere.php');

// ����� �������
class env {
	
	static function debugmode() {
		$GLOBALS['SYSTEM']['DEBUGMODE'] = true;
	}
	
	// ������������� ���� ��� ���� {files}
	static function StartNewCache($path,$cacheid) {
		$GLOBALS['SYSTEM']['CACHE_DIR'] = ROOTDIR . DS . CACHE_ROOT . $path;
		$GLOBALS['SYSTEM']['DEBUG_CAHCHEINUSE'] = true;
		return new bw_cacheonfiles($cacheid);
	}
	
	// �������� ��� - �����
	static function StartNewMemcached($host = '127.0.0.1', $port = 11211, $compress = 0) {
		return new bw_memcached($host, $port, $compress);
	}
	
	// �������� ����� ��������� �������� �� ����������� �����, ���� �������� ������� �������
	static function GetTimePageGeneration($round = 0){
		if (SYSTEM_GENTIME) {
			$end_time = microtime();
			$end_array = explode(" ",$end_time);
			$end_time = $end_array[1] + $end_array[0];
			$ret = $end_time - $GLOBALS['SYSTEM']['SYSTEM_GENTIME'];
			return ($round == 0) ? $ret : round($ret,$round);
		}else return '';
	}
	
	// mysql ������� ����������� �� ��������, �� ������� ����������� ������ � ��
	static function mysql($close = false) { 
		if (!$close) {
			if (!defined('MYSQL_ENGINE') || MYSQL_ENGINE == 'mysql') {
				mysql_connect(MYSQL_HOST,MYSQL_USERNAME,MYSQL_PASSWORD) or die(mysql_error());
				mysql_select_db(MYSQL_BASE); 
			}elseif(MYSQL_ENGINE == 'mysqli') {
				global $dblink;
				$dblink = mysqli_connect(MYSQL_HOST,MYSQL_USERNAME,MYSQL_PASSWORD) or die(mysqli_error());
				mysqli_select_db($dblink, MYSQL_BASE); 
			}
		}else{
			if (!defined('MYSQL_ENGINE') || MYSQL_ENGINE == 'mysql') {
				mysql_close();	
			}elseif(MYSQL_ENGINE == 'mysqli') {
				global $dblink;
				mysqli_close($dblink);
			}
		}
	}
	
	// ����������� ������ 
	static function module($mdl) {
		$mfile = ROOTDIR . DS . MODULES_ROOT . $mdl . DS . 'module.php';
		if (file_exists($mfile)) {
			include_once($mfile);
			return true;
		}else{
			return false;
		}
	}
	
}

// ��� ������ � �������� ��������� ��������
class native {
	
	static public $mainview = false;
	
	static public function header($folder, $arr = array()) {
		self::$mainview = new bw_nativeview(ROOTDIR . DS . BWEB_FOLDER . DS . 'templates' . DS . $folder . DS);
		foreach ($arr as $key => $value) {
			self::$mainview->set($key, $value);
		}		
		self::$mainview->display( 'header.tpl' );	 
	} 
	
	static public function footer($arr = array()) {
		if (self::$mainview != false) {
		foreach ($arr as $key => $value) {
			self::$mainview->set($key, $value);
		}
		self::$mainview->display( 'footer.tpl' );	
		}else echo '<div style="color:red">��� ������ ���������� ���������� header</div>'; 
	}
	
	static public function template($name,$text = '',$arr = array()) {
		self::header($name,$arr);
		print $text;
		self::footer($arr);
	}  
	
	static public function settitle($title) {
		$GLOBALS['NATIVE']['PAGE_TITLE'] = $title;
	}
	
	static public function setmeta($tag,$ext) {
		$GLOBALS['NATIVE']['META'][$tag] = $ext;
	}
	
	static public function addcss($css) {
		$GLOBALS['NATIVE']['CSS'][] = $css;
	}
	
	static public function addjs($js) {
		$GLOBALS['NATIVE']['JS'][] = $js;
	}
	
}

// �������� �������
class language {
	
	static private function langauge_select_by_domain() {
		$ft = trim(strtolower($_SERVER['SERVER_NAME']));
		$host = str_ireplace(DOMAIN,"",$ft);
		if ($host != $ft) { 
			$host = str_replace(".","",$host);
			if ($host == "www") 
				return "ru";
			elseif ($host != "") 
				return $host;
			else return "ru";
		}else return "ru";
	}
	
	static public function determ($how = 'domain') {
		if ($how=='domain') {
			$GLOBALS['NATIVE']['LANGUAGE'] = self::langauge_select_by_domain();
		}
	}
	
	static public function cur() {
		return $GLOBALS['NATIVE']['LANGUAGE'];
	}
	
	static public function is($lng = 'ru') {
		if ($lng == 'ru') {
			return ($GLOBALS['NEW_ICF']['LANGUAGE']=='ru') ? true : false;
		}
	}
	
	static public function m($from,$to,$lng = 'ru') {
		return (self::is($lng)) ? $from : $to;
	}
	
	static public function add($filename) {
		$langfile = ROOTDIR . DS . LANGUAGE_ROOT . DS . self::cur() . DS . $filename;
		//print $langfile;
		if (file_exists($langfile)) {
			include_once($langfile);
			return true;
		}else {
			return false;
		}
	}
	
	static public function getstring($pam) {
		return $GLOBALS['NATIVE']['language_string'][$pam];
	}
	
	static public function compile_language($l) {
		if (is_array($l)) {
			foreach ($l as $key => $value) {
				$GLOBALS['NATIVE']['language_string'][$key] = $value;
			}
		}
	}
	
}

// �������� ������� ����� ��������� ��������
if (SYSTEM_GENTIME) {
	$start_time = microtime();
	$start_array = explode(" ",$start_time);
	$GLOBALS['SYSTEM']['SYSTEM_GENTIME'] = $start_array[1] + $start_array[0];
}

// ��� ��������� �������������... 
include_once('startup_init.php');

?>