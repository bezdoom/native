<?

// ����� ��������� (iusik.ru)
function debugmessage($message, $title = false, $color = "#008B8B") {
	if (!defined('DISABLE_DEBUGMESSAGE')) {
		echo '<table border="0" cellpadding="5" cellspacing="0" style="border:1px solid '.$color.';margin:2px;"><tr><td>';
		if (strlen($title)>0) {
			echo '<p style="color: '.$color.';font-size:11px;font-family:Verdana;">['.$title.']</p>';
		}
		if (is_array($message) || is_object($message)) {
		   echo '<pre style="color:'.$color.';font-size:11px;font-family:Verdana;">'; print_r($message); echo '</pre>';
		}else{
		   echo '<p style="color:'.$color.';font-size:11px;font-family:Verdana;">'.$message.'</p>';
		}
		echo '</td></tr></table>';
	}
}

// �������� ������ ���� ������ � �������� dir
function list_directory($dir) {
   $file_list = ''; $arr = array();
   if ($dh = opendir($dir)) {
      while (($file = readdir($dh)) !== false) {
          if ($file !== '.' AND $file !== '..') {
             $current_file = "$dir".DS."$file";
             if (is_file($current_file)) {
                $arr[] = $file;
             }
          }
       }
   }
   return $arr;
}

// ����������� �������� ���������� �� ���� �� ����������
function RemoveDir($path) {
	if(file_exists($path) && is_dir($path))
	{
		$dirHandle = opendir($path);
		while (false !== ($file = readdir($dirHandle))) 
		{
			if ($file!='.' && $file!='..')// ��������� ����� � ��������� '.' � '..' 
			{
				$tmpPath=$path.'/'.$file;
				chmod($tmpPath, 0777);
				
				if (is_dir($tmpPath))
	  			{  // ���� �����
					RemoveDir($tmpPath);
			   	} 
	  			else 
	  			{ 
	  				if(file_exists($tmpPath))
					{
						// ������� ���� 
	  					unlink($tmpPath);
					}
	  			}
			}
		}
		closedir($dirHandle);
		
		// ������� ������� �����
		if(file_exists($path))
		{
			rmdir($path);
		}
	}
}

// ������ ������������ ��������� ��� ��������� �������� ����� ����
function ja2ans($s){
	return preg_replace('#%u([0-9A-F]{4})#se',
	                  'iconv("UTF-16BE","Windows-1251",pack("H4","$1"))',
	                  $s);
}

// ������ ������� �� �� ������ ������������� ��� html
function quotefix($string){
   return str_replace("\"","&quot;",$string);
}

// ��������� �����
function random($min,$max){
	mt_srand(time()+(double)microtime()*10000000);
	return mt_rand($min,$max-1);
}

// ���������� ���������� ������������� ��� ��� 
function unicid(){
	$better_token = md5(uniqid(random(0,Rand())));
	return $better_token;
}

// ������� ����� � �����
function kill_cache($cachepath)	{
	$pa = ROOTDIR . DS . CACHE_ROOT . $cachepath;
	RemoveDir($pa);
}

// �������� ����� �� ������ �����������
// ������: 
// $num1=2;
// $words1=Array("�����", "�����", "������");
function inflect($num,$words) {
  $num=$num%100;
  if ($num>19) { $num=$num%10; }
  switch ($num) {
    case 1:  { return($words[0]); }
    case 2: case 3: case 4:  { return($words[1]); }
    default: { return($words[2]); }
  }
}
// �������������
function inflect2($number, $titles){
    $cases = array (2, 0, 1, 1, 1, 2);
    return $titles[ ($number%100>4 && $number%100<20)? 2 : $cases[min($number%10, 5)] ];
}

// �������� �������� �������� ������������
function getRealIP(){
 if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
   $ip=$_SERVER['HTTP_CLIENT_IP'];
 }
 elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
   $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
 }
 else{
   $ip=$_SERVER['REMOTE_ADDR'];
 }
 return $ip;
}

// ������ ������ ������ ��� ����� � ������� ������� limit �������� 
function MakeTextLine($src,$limit = 400,$addline = "...") {
$src = str_replace("\r\n", " ", $src);
$src = str_replace("\n", " ", $src);
$src = str_replace("\r", " ", $src);
preg_match("/([\w\W]{0,".$limit."})([\w\W]*)/", strip_tags(trim(ereg_replace(" +"," ",$src))), $out); 
if (strlen($out[1]) != strlen($src)) 	
	return trim($out[1].$addline); 
elseif(strlen($out[1]) == strlen($src)) 
	return trim($out[1]); 	
}

// �������� ������� ����� � ������
function packLongWords($input,$max_word_length = 32) {
	return preg_replace('/([^\s]{'.$max_word_length.'})[^\s]+/', '<span title="$0">$1...</span>', $input);
}

// �������� ��������� �� �������
function ShowError($arError,$str = '��������� ������ ��������� �� ����� ������ �������:',$style = 'border:1px solid red; padding:5px; margin:3px; color:red; clear:both') {
	if (is_array($arError) && !empty($arError) && strval($str)) {
		foreach ($arError as $key => $value) {
			$mess .= "<li>".$value['ERROR']."</li>\r\n";
		}	
		return "<div style=\"".$style."\"><strong>".$str."</strong>\r\n<ol>\r\n".$mess."</ol></div>\r\n";
	}
}

// ������ �� ���� ���� YYYY-MM-DD HH:MI:SS �����
	// strext 
	// m - minuts
	// h - hours
	// s - seconds
	// d - day
	// M - month (sM) - short month (tM) - full month
	// y - year
function dataExtract($in, $strext = 'h:m') {
	global $NameMonth;
	//debugmessage($in);

	$ar = explode(' ',$in);
	$da = explode('-',$ar[0]);
	$tm = explode(':',$ar[1]);
	//debugmessage($tm);
	return str_replace(
			array(
				'tM', 'sM',
				'h', 'm', 's',
				'd', 'M', 'y',
			),
			array(
				$NameMonth[$da[1]], $NameMonth['s_'.$da[1]],
				$tm[0], $tm[1], $tm[2],
				$da[2], $da[1], $da[0],
			),
			$strext
		);
}

// ���������� �������
function paginator($records,$r_start,$URL,$inpage,$end = "",$class = "",$separator = "|"){
 $str="";

 if($class <> ""){
  $class = " class=\"$class\"";
 }

 if ($records<=$inpage) return;
 if ($r_start!=0){
  $str.="<a$class href=".$URL."0$end>&lt;&lt</a> ";
  $str.="<a$class href=$URL".($r_start-1)."$end>&lt;</a> ";
 }else $str.="&lt;&lt &lt;</font> ";

 if ($r_start==0) {$sstart=$r_start-0;$send=$r_start+10;}
 if ($r_start==1) {$sstart=$r_start-1;$send=$r_start+9;}
 if ($r_start==2) {$sstart=$r_start-2;$send=$r_start+8;}
 if ($r_start==3) {$sstart=$r_start-3;$send=$r_start+7;}
 if ($r_start==4) {$sstart=$r_start-4;$send=$r_start+6;}
 if ($r_start>=5) {$sstart=$r_start-5;$send=$r_start+5;}

 if ($send*$inpage>$records)$send=$records/$inpage;
 if ($sstart<0) $sstart=0;

 if ($records%$inpage==0) $add=0; else $add=1;

 for ($i=$sstart;$i<$send;$i++){
  if ($i==$r_start) $str.=" <b>".($i+1)."</b> $separator ";
  else $str.="<a$class href=$URL".($i)."$end><u><b>".($i+1)."</b></u></a> $separator  ";
 }

 if ($r_start+(1-$add)<intval($records/$inpage)){
  $str.=" <a$class href=$URL".($r_start+1)."$end>&gt;</a>";
  $str.=" <a$class href=$URL".(intval($records/$inpage)-(1-$add))."$end>&gt;&gt;</a>";
 }else $str.=" &gt; &gt;&gt";

 return $str;
}

// ��������� ������
function checkurl($url) {
   // ����� ����� ������� � ������� �������
   $url=strtolower(trim($url));
   // ���� ����� - �����
   if (strlen($url)==0) return 0;
   //��������� ��� �� ������������
   if (!preg_match("~^(?:(?:https?|ftp|telnet)://(?:[a-z0-9_-]{1,32}".
   "(?::[a-z0-9_-]{1,32})?@)?)?(?:(?:[a-z0-9-]{1,128}\.)+(?:com|net|".
   "org|mil|edu|arpa|gov|biz|info|aero|inc|name|[a-z]{2})|(?!0)(?:(?".
   "!0[^.]|255)[0-9]{1,3}\.){3}(?!0|255)[0-9]{1,3})(?:/[a-z0-9.,_@%&".
   "?+=\~/-]*)?(?:#[^ '\"&<>]*)?$~i",$url,$ok))
   return 0; // ���� �� ��������� - �����
   // ���� ��� ��������� - ��������
   if (!strstr($url,"://")) $url="http://".$url;
   // �������� �������� �� ������ �������: hTtP -> http
   //$url=preg_replace("~^[a-z]+~ie","strtolower('\\')",$url);
   return $url;
}

?>