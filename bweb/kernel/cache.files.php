<?php
// ���� � ����������: larin.in

interface I_BWCORE_filecache {
	public function save($value, $valueID);
	public function load($valueID, $time);
	public function delete($valueID);
	public function clearcache($cachepath);
}

class bw_cacheonfiles implements I_BWCORE_filecache{
	
	private $path;

	public function __construct($path = 'cache')
	{
		$this->path = $path;
	}

	public function save($value, $valueID)
	{
		$str_val = serialize($value);
		$file_name = $this->pathCache($valueID) .
			$this->nameCache($valueID);
		$f = fopen($file_name, 'w+');
		if (flock($f, LOCK_EX)) {
			fwrite($f, $str_val);
			flock($f, LOCK_UN);
		}
		fclose($f);
		unset($str_val);
		$GLOBALS['SYSTEM']['DEBUG_CAHCHEINUSE'] = false;
	}
	
	public function load($valueID, $time)
	{
		$file_name = $this->getPathCache($valueID) .
			$this->nameCache($valueID);
		if (!file_exists($file_name)) return false;
		if ((filemtime($file_name) + $time) < time()) {
			return false;
		}
		if (!$data = file($file_name))  return false;
		$GLOBALS['SYSTEM']['DEBUG_CAHCHEINUSE'] = true;
		return unserialize(implode('', $data));
	}

	public function delete($valueID)
	{
		$file_name = $this->getPathCache($valueID) .
			$this->nameCache($valueID);
		unlink($file_name);
	}

	private function pathCache($valueID)
	{
		$md5 = $this->nameCache($valueID);
		$first_literal = array($md5{0}, $md5{1}, $md5{2}, $md5{3});
		$path = $this->path . DS;
		foreach ($first_literal as $dir) {
			$path .= $dir . DS;
			if (!file_exists($GLOBALS['SYSTEM']['CACHE_DIR'] . $path)) {
				if (!mkdir($GLOBALS['SYSTEM']['CACHE_DIR'] . $path, 0777, true)) return false;
			}
		}
		return $GLOBALS['SYSTEM']['CACHE_DIR'] . $path;
	}

	private function getPathCache($valueID)
	{
		$md5 = $this->nameCache($valueID);
		$first_literal = array($md5{0}, $md5{1}, $md5{2}, $md5{3});
		return $GLOBALS['SYSTEM']['CACHE_DIR'] . $this->path . DS .
			implode(DS, $first_literal) . DS;
	}

	private function nameCache($valueID)
	{
		return md5($valueID);
	}
	
	public function clearcache($cachepath)
	{
		$pa = ROOTDIR . DS . CACHE_ROOT . $cachepath;
		RemoveDir($pa);
	}
	
}

?>