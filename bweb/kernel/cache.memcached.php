<?

interface I_BWCORE_memcached {
	public function save($value, $valueID);
	public function load($valueID, $timeLife);
	public function delete($valueID);
}

class bw_memcached implements I_BWCORE_memcached
{
	private $memcache;
	private $timeLife;
	private $compress;
 
	/**
	 *
	 * @param string $host - ���� ������� memcached
	 * @param int $port - ���� ������� memcached
	 * @param int $compress - [0,1], ������� ��� ��� ������ �����
	 * ���������� � ������
	 */
	public function __construct($host, $port = 11211, $compress = 0)
	{
		$this->memcache = memcache_connect($host, $port);
		$this->compress = ($compress) ? MEMCACHE_COMPRESSED : 0;
	}

	public function load($valueID, $timeLife)
	{
		$GLOBALS['SYSTEM']['DEBUG_CAHCHEINUSE'] = true;
		$this->timeLife = $timeLife;
		return memcache_get($this->memcache, $valueID);
	}

	public function save($value, $valueID)
	{
		$GLOBALS['SYSTEM']['DEBUG_CAHCHEINUSE'] = false;
		return memcache_set($this->memcache, $valueID, $value, $this->compress, $this->timeLife);
	}

	public function delete($valueID)
	{
		memcache_delete($this->memcache, $valueID);
	}

	public function __destruct()
	{
		memcache_close($this->memcache);
	}
}

?>