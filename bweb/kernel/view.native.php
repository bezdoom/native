<?php
// ���� � ����������: larin.in

class bw_nativeview {
	private $_path;
	private $_template;
	private $_var = array();

	public function __construct($path = '')
	{
		$this->_path = /*$_SERVER['DOCUMENT_ROOT'] .*/ $path;
	}

	public function set($name, $value)
	{
		$this->_var[$name] = $value;
	}

	public function __get($name)
	{
		if (isset($this->_var[$name])) return $this->_var[$name];
		return '';
	}

	public function display($template, $strip = true)
	{
		$this->_template = $this->_path . $template;
		if (!file_exists($this->_template)) die('������� ' . $this->_template . ' �� ����������!');

		ob_start();
		include($this->_template);
		echo ($strip) ? $this->_strip(ob_get_clean()) : ob_get_clean();
	}

	private function _strip($data)
	{
		$lit = array("\\t", "\\n", "\\n\\r", "\\r\\n", "  ");
		$sp = array('', '', '', '', '');
		return str_replace($lit, $sp, $data);
	}

	static public function xss($data)
	{
		if (is_array($data)) {
			$escaped = array();
			foreach ($data as $key => $value) {
				$escaped[$key] = $this->xss($value);
			}
			return $escaped;
		}
		return htmlspecialchars($data, ENT_QUOTES);
	}
	
	public function title($notitle = IF_EMPTY_TITLE) {
		print isset($GLOBALS['NATIVE']['PAGE_TITLE']) ? $GLOBALS['NATIVE']['PAGE_TITLE'] : $notitle;
	}
	
	public function header() {
		// ���������� �������������� ����-����
		if (is_array($GLOBALS['NATIVE']['META']) && !empty($GLOBALS['NATIVE']['META'])) {
			foreach ($GLOBALS['NATIVE']['META'] as $metaname => $metavalue) {
				if (!empty($metaname) && !empty($metavalue)) {
					print "\t<meta name=\"".$metaname."\" content=\"".$metavalue."\" />\r\n";
				}
			}
		}
		// ���������� �������������� �����
		if (is_array($GLOBALS['NATIVE']['CSS']) && !empty($GLOBALS['NATIVE']['CSS'])) {
			foreach ($GLOBALS['NATIVE']['CSS'] as $kk => $vl) {
				if (!empty($vl)) {
					print "\t<link href=\"".$vl."\" rel=\"stylesheet\" type=\"text/css\" />\r\n";
				}
			}
		}
		// ���������� �������������� 
		if (is_array($GLOBALS['NATIVE']['JS']) && !empty($GLOBALS['NATIVE']['JS'])) {
			foreach ($GLOBALS['NATIVE']['JS'] as $kk => $vl) {
				if (!empty($vl)) {
					print "\t<script type=\"text/javascript\" src=\"".$vl."\"></script>\r\n";
				}
			}
		}
	}
	
}
?>