<?

if (!env::module('imageworx')) return false;

class operations {
	
	// �������� �����������
	static function upload_images($files,$params) {

		if (is_array($files) && is_array($params) && count($files)>0) { 	
			
			$arReturn = array();
			
			// ���������
			$params['rename'] = isset($params['rename']) ? ($params['rename']) : (true);
			$params['rename_mask'] = isset($params['rename_mask']) ? ($params['rename_mask']) : ('uniqid');	
			$params['thb_prefix'] = isset($params['thb_prefix']) ? ($params['thb_prefix']) : ('thb_');	
			
			// 	���������� ��� ������������ ������
			$params['upload_dir'] = isset($params['upload_dir']) ? ($params['upload_dir']) : (UPLOAD_ROOT);	
			$inBackOF_path = $params['upload_dir'];
			$params['upload_dir'] = ROOTDIR . DS . $params['upload_dir'];	
			if (!file_exists($params['upload_dir'])) {
				if (!mkdir($params['upload_dir'], 0777, true)) return false;
			}
			
			// ���������� ��� ���������� �����
			$params['upload_thb_dir'] = isset($params['upload_thb_dir']) ? ($params['upload_thb_dir']) : (UPLOAD_ROOT . 'thumbs' . DS);
			$inBackTF_path = $params['upload_thb_dir'];
			$params['upload_thb_dir'] = ROOTDIR . DS . $params['upload_thb_dir'];
			if (!file_exists($params['upload_thb_dir'])) {
				if (!mkdir($params['upload_thb_dir'], 0777, true)) return false;
			}
			
			// ������ ���������
			if (is_array($params['original_size']) && isset($params['original_size'])) {
				$params['original_size']['width'] = intval($params['original_size']['width']) ? ($params['original_size']['width']) : 0;
				$params['original_size']['height'] = intval($params['original_size']['height']) ? ($params['original_size']['height']) : 0;
				
				$can_use_original_resize = true;
				if ($params['original_size']['width'] == 0 && $params['original_size']['height']==0) {
					$can_use_original_resize = false;
				}
			}else{
				$can_use_original_resize = false;
			}
			
			// ������ ����������� �����
			if (is_array($params['thumb_size']) && isset($params['thumb_size'])) {
				$params['thumb_size']['width'] = intval($params['thumb_size']['width']) ? ($params['thumb_size']['width']) : 165;
				$params['thumb_size']['height'] = intval($params['thumb_size']['height']) ? ($params['thumb_size']['height']) : 0;
				
				$can_use_thumbnail = true;
				if ($params['thumb_size']['width'] == 0 && $params['thumb_size']['height']==0) {
					$can_use_thumbnail = false;
				}
			}else{
				$can_use_thumbnail = false;
			}
			
			// ������� �� ����� �� ������� ���������� ������
			for($i = 0; $i < count($files)-1; $i++)	{
				if ($files['name'][$i] != false) {
					
					if ($params['rename']) {
						$fileinfo = @pathinfo($params['upload_dir'].$files['name'][$i]);
						if ($params['rename_mask'] == 'uniqid') {
							$new_filename = unicid().".".$fileinfo['extension'];
						}else{
							$new_filename = date($params['rename_mask'])."_".$i.".".$fileinfo['extension'];
						}
					}else {
						$new_filename = $files['name'][$i];
					}

					if ($can_use_thumbnail) {
						$new_thb_filename = $params['thb_prefix'].$new_filename;
					}
					
					// ������ ������ ����
					$const_NEWFN = $new_filename;
					$const_NEW_THB_FN = $new_thb_filename;	
									
					$new_filename = $params['upload_dir'].$new_filename;
					$new_thb_filename = $params['upload_thb_dir'].$new_thb_filename;
					
					if (!file_exists($new_filename)) {
							// �������� � ����������� ������
							if ($can_use_thumbnail) {
								imageworx::resampled($files['tmp_name'][$i], $new_thb_filename,  $params['thumb_size']['width'], $params['thumb_size']['height']);
								$arReturn[$i]['THUMB'] = array(
									'FILE_NAME' => $const_NEW_THB_FN,
									'FILE_PATH' => $inBackTF_path,
									'FULL' => $inBackTF_path.$const_NEW_THB_FN,
								);
							}
													
							// �������� � ����������
							if ($can_use_original_resize) {
								imageworx::resampled($files['tmp_name'][$i], $new_filename,  $params['original_size']['width'], $params['original_size']['height']);
							}else{
								move_uploaded_file($files['tmp_name'][$i],$new_filename);
							}
							
							$arReturn[$i]['ORIGINAL'] = array(
								'FILE_NAME' => $const_NEWFN,
								'FILE_PATH' => $inBackOF_path,
								'FULL' => $inBackOF_path.$const_NEWFN
							);
							
							// ������� ��������� ����
							@unlink($files['tmp_name'][$i]); 
						}
						
					}
					
				}
				
				return $arReturn;
				
			}else return false;
			
		}
	
}
	
?>