<? 

class wikistuff {
	
	// ���������� ���������� ��������� �� ������� (��� � ���������)
	public $LAST_ERROR = '';
	public $ERRORS = array();
	
	// ��������� ������������� - �������� �� ���������
	private $arParams = array(
		'TEXT_DECORATE' => true, 	// ���������� ������
		'HREF' => true,				// ��������� ������
		'TABLES' => true,			// �������
		'CUT' => false,				// ��� CUT
		'PARAGRAPH' => true,		// ���������
		'ADDITIONAL' => true,		// �������������� ���� (������� ������, ���������� � ��)
		'VIDEO' => true,			// ������� ����� � ����� � ��������
		'LISTS' => true, 			// ������������� ������
		'IMAGES' => true,			// ����������� ��������
		'HEADERS' => true,			// ���������
		'CODE' => true,				// �������� ��� ��� ��������� ������� ���� (��� ���������)
		'GESHI' => false,			// ��������� ���������� (���������� GESHI)
		'TABS2STRING' => true,		// �������� �� 5 ��������
	);
	
	// ����������� �������
	function __construct($init_params = array(),$only_init = false) {
		// ������������� ��������� ������� (���� ������� ���������)
		if (is_array($init_params) && !empty($init_params)) {
			if (!$only_init) {
				foreach($init_params as $opt => $value) {
					$this->arParams[$opt] = $value;
				}
			}else{
				$this->arParams = $init_params;
			}
		}
		
		// �������������� ���������� Geshi , ���� ����� ���������
		if ($this->arParams['GESHI']) {
			require_once("geshi/geshi.php");
		}
	}
	
	// ������� ���������� ��������� �� ������
	private function error($message) {
		if (!empty($message) && strval($message)) {
			$this->ERRORS[] = array('MESSAGE'=>$message,'DATETIME'=>date('Y-m-d H:m:s'));
			$this->LAST_ERROR = $message; 
		}
	}
	
	// �������� ���������, � �������� ��������������� ������
	public function ShowInitParams($nl2br = false) {
		if (is_array($this->arParams)) {
			if (!empty($this->arParams)) {
				foreach($this->arParams as $name => $opt) {
					if (($opt) || (is_array($opt) && !empty($opt)))
					$returnText .= "\t".$name."\r\n";
				}
				return ($nl2br) ? nl2br($returnText) : $returnText;
			}else{$this->error('��������� ������������� �����!');}
		}else{$this->error('���������� �������� ��������� �������������, ��� ��� � ��������� � �������� �������!');}
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////// �������� ���������� �������������� ///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	///////////////////
	///////////// GESHI
	///////////////////
	
	// ������������ ���� � ��������� Geshi, ���� ������� �������
	private function geshiGo($code, $language) {
		if ($this->arParams['GESHI']) {
			$find1 = array('&amp;', '&#123;', '&lt;', '&gt;', '&#036;', '&#039;', '&quot;', '&#92;', '&amp;amp;', '&amp;nbsp;');
			$replace1 = array('&', '{', '<', '>' , '$', '\'', '"', '\\', '&amp;', '&nbsp;');	
			$code = str_replace($find1, $replace1, $code);			
			$geshi = new GeSHi($code, $language);
			$geshi->set_header_type(GESHI_HEADER_DIV);		
			$code = $geshi->parse_code();
			$code = preg_replace('/&amp;#([0-9]+);/', '&#$1;', $code);
			$code = str_replace(array('[', ']', "\n"), array('[', ']', ''), $code);
		}
		return $code;
	}
	
	// ����� �� ��������� ��������, ��������� ��������� ����������
	private function geshiPregCheck($string) {
		if ($this->arParams['GESHI']) {
			$search = '/\{code:(.*?)\}\r?\n?(.*?)\r?\n?\{\/code\}/is';
			return preg_replace_callback($search, array('self', 'geshiApplyInFound'), $string);
		}else{
			return $string;
		}
	}
	
	// ���������� ��������� �� �������� ���������
	private function geshiApplyInFound($data) { 
		$syntax = $data[1];
		$code = $data[2];
		if ($this->arParams['GESHI']) {
			return '<blockquote class="code">'.$this->geshiGo($code,$syntax).'</blockquote>';
		}else{
			return $code;
		}
	}
	
	////////////////////
	/////////////// CODE
	////////////////////
	
	// �������� ��������� ���� ��� ���������
	private function applyCODEtag($string) {
		if ($this->arParams['CODE']) {
			$s='/\{code\}(.+?)\{\/code\}/s'; 
			do { 
				if($num = preg_match($s, $string, $out)){
					$o = (($this->geshiGo(trim($out[1]),"text")));
					$r='<blockquote class="code">'.$o.'</blockquote>';
					$string = preg_replace($s,$r,$string);		
				}
			} while ( $num != 0 );
		}
		return $string;   
	}
	
	////////////////////
	////////////// LISTS
	////////////////////
	
	// ����������� ������
	private function list1preg($text) {
		if ($this->arParams['LISTS']) {
			$search = '/\{\#(.+?)\#\}/is';
			return preg_replace_callback($search, array('self', 'applylist1'), $text);
		}else{
			return $text;
		}
	}
	
	// ������ ��� �������
	private function list2preg($text) {
		if ($this->arParams['LISTS']) {
			$search = '/\{\*(.+?)\*\}/is';
			return preg_replace_callback($search, array('self', 'applylist2'), $text);
		}else{
			return $text;
		}
	}
	
	private function applylist1($data) {
		if ($this->arParams['LISTS']) {
			$arrayBBcode=array(
				"/\s\#(.+?)\\n/s"=>"<li>\\1</li>\r\n"
			);	
			$string = $data[1];	
			foreach($arrayBBcode as $search => $replace){
				do { 
					if($num = preg_match($search, $string)){
						$string = preg_replace($search,$replace,$string);
					}
				} while ( $num != 0 );
			} 
			return "<ol>\r\n".$string."\r\n</ol>";
		}
	}
	
	private function applylist2($data) {
		if ($this->arParams['LISTS']) {
			$arrayBBcode=array(
				"/\s\*(.+?)\\n/s"=>"<li>\\1</li>\r\n"
			);	
			$string = $data[1];	
			foreach($arrayBBcode as $search => $replace){
				do { 
					if($num = preg_match($search, $string)){
						$string = preg_replace($search,$replace,$string);
					}
				} while ( $num != 0 );
			} 
			return "<ul>\r\n".$string."\r\n</ul>";
		}
	}
	
	////////////////////
	///////////// TABLES
	////////////////////
		
	private function tableWIKIpreg($text) {
		if ($this->arParams['TABLES']) {
			$search = '/\{\|\|(.+?)\|\|\}/s';
			return preg_replace_callback($search, array('self', 'applytableWIKI'), $text);
		}else{
			return $text;
		}
	}
	
	private function tableNOBRpreg($text) {
		if ($this->arParams['TABLES']) {
			$search = '/\{noborder\|\|(.+?)\|\|\}/s';
			return preg_replace_callback($search, array('self', 'applytableNOBR'), $text);
		}else{
			return $text;
		}
	}
	
	private function applytableWIKI($data) {
		if ($this->arParams['TABLES']) {
			$arrayBBcode=array(
				"/\!!--(.+?)\--!!/s"=>"<tr>\\1</tr>", 		
				"/\[\|(.+?)\|\]/s"=>"<td>\\1</td>", 		
				"/\[\!(.+?)\!\]/s"=>"<th>\\1</th>", 		
				"/\[t\|(.+?)\|\]/s"=>"<td valign=\"top\">\\1</td>", 		
				"/\[t\!(.+?)\!\]/s"=>"<th valign=\"top\">\\1</th>", 		
				"/\{\!(.+?)\!\}/s"=>"<caption>\\1</caption>", 
			);	
			$string = $data[1];	
			foreach($arrayBBcode as $search => $replace){
				do { 
					if($num = preg_match($search, $string)){
						$string = preg_replace($search,$replace,$string);
					}
				} while ( $num != 0 );
			} 
			return "<table class=\"wiki\">".$string."</table>";
		}
	}
	
	private function applytableNOBR($data) {
		if ($this->arParams['TABLES']) {
			$arrayBBcode=array(
				"/\!!--(.+?)\--!!/s"=>"<tr>\\1</tr>", 		
				"/\[\|(.+?)\|\]/s"=>"<td>\\1</td>", 		
				"/\[\!(.+?)\!\]/s"=>"<th>\\1</th>", 		
				"/\[t\|(.+?)\|\]/s"=>"<td valign=\"top\">\\1</td>", 		
				"/\[t\!(.+?)\!\]/s"=>"<th valign=\"top\">\\1</th>", 		
				"/\{\!(.+?)\!\}/s"=>"<caption>\\1</caption>", 
			);	
			$string = $data[1];	
			foreach($arrayBBcode as $search => $replace){
				do { 
					if($num = preg_match($search, $string)){
						$string = preg_replace($search,$replace,$string);
					}
				} while ( $num != 0 );
			} 
			return "<table class=\"noborder\">".$string."</table>";
		}
	}
	
	////////////////////
	///////////// OTHERS
	////////////////////
	
	// ��������� � �������
	private function tabs2string($string){
		if ($this->arParams['TABS2STRING']) {
			return str_replace("\t","     ",$string);
		}else{
			return $string;
		}
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//////// ��������� ����� ��������  ////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	// ���������� �����
	public function parse($string) {
		// TABS2STRING => true
		if ($this->arParams['TABS2STRING']) {
			$string = $this->tabs2string($string);
		}
		// GESHI => true
		if ($this->arParams['GESHI']) {
			$string = $this->geshiPregCheck($string);
		}
		// CODE => true
		if ($this->arParams['CODE']) {
			$string = $this->applyCODEtag($string);
		}
		// LISTS => true
		// ������ �����
		if ($this->arParams['LISTS']) {			
			$string = $this->list1preg($string);
			$string = $this->list2preg($string);
		}
		// TABLES => true
		// ������ �����
		if ($this->arParams['TABLES']) {
			$string = $this->tableWIKIpreg($string);
			$string = $this->tableNOBRpreg($string);
		}
		
		////////////////////////////////////////
		// HREF => true
		if ($this->arParams['HREF']) {
			$o["/\[\[(.+?)\\]\[(.+?)\\]\]/s"] = "<a href=\"\\1\">\\2</a>";
			$o["/\[\[(.+?)\\]\]/s"] = "<a href=\"\\1\">\\1</a>";
		}
		// TEXT_DECORATE => true
		if ($this->arParams['TEXT_DECORATE']) {
			$o["/\''''(.+?)\''''/s"] = "<u>\\1</u>";
			$o["/\'''(.+?)\'''/s"] = "<strong>\\1</strong>";
			$o["/\''(.+?)\''/s"] = "<em>\\1</em>";
		}
		// CUT => true
		if ($this->arParams['CUT']) {
			$o["/\{cut}/s"] = "";
		}
		// ADDITIONAL => true
		if ($this->arParams['ADDITIONAL']) {
			$o["/\ - /s"] = " � ";
			$o["/\{br}/s"] = "<br />";
			$o["/\{both}/s"] = "<div style=\"clear: both;\"></div>";
			$o['/\{cen\}(.+?)\{\/cen\}/s'] = '<center>\\1</center>';
			$o['/\{center\}(.+?)\{\/center\}/s'] = '<center>\\1</center>';
		}
		// HEADERS => true
		if ($this->arParams['HEADERS']) {
			$o["/\=====(.+?)\=====/s"] = "<h1 class=\"wiki_h1\">\\1</h1>";
			$o["/\===(.+?)\===/s"] = "<h3 class=\"wiki3\">\\1</h3>";
			$o["/\==(.+?)\==/s"] = "<h3 class=\"wiki2\">\\1</h3>";
		}
		// PARAGRAPH => true
		if ($this->arParams['PARAGRAPH']) {
			$o["/\````(.+?)\````/s"] = "<p style=\"margin-top:3px; margin-bottom:7px; \" align=\"justify\">\\1</p>";
			$o["/\```(.+?)\```/s"] = "<p style=\"margin-top:3px; margin-bottom:7px; \">\\1</p>";
		}
		// IMAGES => true
		if ($this->arParams['IMAGES']) {
			$o["/\{n:(.+?)\}/s"] = "<div style=\"float: none;\" class=\"shadow\"><a rel=\"prettyPhoto\" href=\"/uploads/images/wiki/\\1\"><img src=\"/uploads/images/wiki/thumbs/thb_\\1\" border=\"0\" /></a></div>\r\n";
			$o["/\{r:(.+?)\}/s"] = "<div style=\"float: right;\" class=\"shadow\"><a rel=\"prettyPhoto\" href=\"/uploads/images/wiki/\\1\"><img src=\"/uploads/images/wiki/thumbs/thb_\\1\" border=\"0\" /></a></div>\r\n";
			$o["/\{l:(.+?)\}/s"] = "<div style=\"float: left;\" class=\"shadow\"><a rel=\"prettyPhoto\" href=\"/uploads/images/wiki/\\1\"><img src=\"/uploads/images/wiki/thumbs/thb_\\1\" border=\"0\" /></a></div>\r\n";		
			$o["/\{on:(.+?)\}/s"] = "<img style=\"margin:2px; padding:2px\" src=\"/uploads/images/wiki/\\1\" border=\"0\" />\r\n";
			$o["/\{or:(.+?)\}/s"] = "<img align=\"right\" style=\"margin:2px; padding:2px\" src=\"/uploads/images/wiki/\\1\" border=\"0\" />\r\n";
			$o["/\{ol:(.+?)\}/s"] = "<img align=\"left\" style=\"margin:2px; padding:2px\" src=\"/uploads/images/wiki/\\1\" border=\"0\" />\r\n";		
			$o["/\{fon:(.+?)\}/s"] = "<img style=\"margin:0px; padding:0px\" src=\"/uploads/images/wiki/\\1\" border=\"0\" />\r\n";
			$o["/\{img:(.+?)\}/s"] = "<img style=\"margin:0px; padding:0px\" src=\"\\1\" border=\"0\" />\r\n";
		}
		// VIDEO => true
		if ($this->arParams['VIDEO']) {
			$o["/\{youtube:(.+?)\}/s"] = "<div style=\"margin-top:3px; margin-bottom:5px;\"><object type=\"application/x-shockwave-flash\" data=\"http://www.youtube.com/v/\\1\" width=\"425px\" height=\"350px\"><param name=\"movie\" value=\"http://www.youtube.com/v/\\1\"><param name=\"scale\" value=\"noScale\"><param name=\"wmode\" value=\"window\"></object></div>";
			$o["/\{vk:(.+?)_(.+?)_(.+?)\}/s"] = "<div style=\"margin-top:3px; margin-bottom:5px;\"><iframe src=\"http://vkontakte.ru/video_ext.php?oid=\\1&id=\\2&hash=\\3\" width=\"607\" height=\"360\" frameborder=\"0\"></iframe></div>";			
		}
		
		// ������ � ������
		foreach($o as $search => $replace){
			$list1 = 0; $list1 = 0;
			do { 
				if($num = preg_match($search, $string)){
					$string = preg_replace($search,$replace,$string);
				}
			} while ( $num != 0 );
		} 
		////////////////////////////////////////
		
		// LISTS => true
		// ������ �����
		if ($this->arParams['LISTS']) {			
			$string = $this->list1preg($string);
			$string = $this->list2preg($string);
		}
		// TABLES => true
		// ������ �����
		if ($this->arParams['TABLES']) {
			$string = $this->tableWIKIpreg($string);
			$string = $this->tableNOBRpreg($string);
		}
		
		return $string;
	}
	
}

?>