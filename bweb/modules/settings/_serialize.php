<?

class BWSettingsSerialize {
	
	// ��������� ��������� �� ������
	public $LAST_ERROR = '';
	
	// ������ � �������� �����������
	private $CurrentSettingsArray;
	
	// ��������������� �������������� ����������
	private $currentCategory;  // ������� �������� �������
	
	///////////////////////////////////////////////////////////////////////
	//////// �����������
	///////////////////////////////////////////////////////////////////////
	
	function __construct($start = null) {
        // �������� ��� �������� �������
		if ($start != null) {
			$this->Load($start);
		} else {
			$this->CurrentSettingsArray = array();
		}
		// ���������� ��������� ����������
		$this->currentCategory = '';
    }
	
	///////////////////////////////////////////////////////////////////////
	//////// ������� �������� � ���������� ��������
	///////////////////////////////////////////////////////////////////////
	
	// ��������� ��������� � ������� �������
	// ���������� ��������������� ������ � �����������
	public function Save() {
		if (is_array($this->CurrentSettingsArray) && !empty($this->CurrentSettingsArray)) {
			return serialize($this->CurrentSettingsArray);
		}else{
			$this->LAST_ERROR = '��������� ��� ����������. ������ ���� ��� �� ����������.';
			return false;
		}
	}
	
	// ��������  ��������� � ������
	// �� ���� ��������������� ������
	public function Load($ser) {
		$this->CurrentSettingsArray = unserialize($ser);
		if (is_array($this->CurrentSettingsArray) && !empty($this->CurrentSettingsArray)) {
			return true;
		}else{
			$this->LAST_ERROR = '��������� ��� ��������. ������ ���� ��� �� ����������.';
			return false;
		}
	}
	
	///////////////////////////////////////////////////////////////////////
	//////// ������� �������� � ���������� ��������
	///////////////////////////////////////////////////////////////////////
	
	// ��������� ������� ���������
	public function SetCategory($catName) {
		if (isset($this->CurrentSettingsArray[$catName])) {
			$this->currentCategory = $catName;
		}else{
			$this->LAST_ERROR = '��������� "'.$catName.'" �� ����������.';
		}
	}
	
	// �������� ������� ���������
	public function GetCategory() {
		return $this->currentCategory;
	}
	
	// ��������� ����� ��������� � ���������
	public function AddCategory($catName) {
		if (!isset($this->CurrentSettingsArray[$catName])) {
			$this->CurrentSettingsArray[$catName] = array();
			$this->SetCategory($catName);
		}else{
			$this->LAST_ERROR = '��������� "'.$catName.'" ��� ����������.';
		}
	}
	
	// �������� ��� �������� � ���������
	public function GetPropertyList($catName = false) {
		$ret = array();
		if (!$catName) $catName = $this->GetCategory();
		if (isset($this->CurrentSettingsArray[$catName]) && is_array($this->CurrentSettingsArray[$catName])) {
			if (!empty($this->CurrentSettingsArray[$catName])) {
				foreach ($this->CurrentSettingsArray[$catName] as $name => $value) {
					$ret[] = $name;
				}
			}else{
				$this->LAST_ERROR = '��������� "'.$catName.'" ����� ��� �� ����������.';
			}	
		}else{
			$this->LAST_ERROR = '�� ���������� ��������� ������ �������, ��� ��� ������ �������� ������ �� ��������������.';
		}		
		return $ret;
	}
	
	// �������� ������ ���������
	public function GetCategoryList() {
		$ret = array();
		if (isset($this->CurrentSettingsArray) && is_array($this->CurrentSettingsArray)) {
			if (!empty($this->CurrentSettingsArray)) {
				foreach ($this->CurrentSettingsArray as $name => $value) {
					$ret[] = $name;
				}
			}else{
				$this->LAST_ERROR = '�� ����� ���������  �� ���������.';
			}	
		}else{
			$this->LAST_ERROR = '�� ���������� ��������� ������ ���������, ��� ��� ������ �������� ������ �� ��������������.';
		}		
		return $ret;
	}
	
	// ���������� �� �������� � �������
	public function IsExistsProperty($name,$cat = false) {
		if (!$cat) $cat = $this->currentCategory;
		return strval($cat) ? isset($this->CurrentSettingsArray[$cat][$name]) : false;
	}
	
	// ���������� �� �������
	public function IsExistsCatalog($name = false) {
		if (!$name) $name = $this->currentCategory;
		return isset($this->CurrentSettingsArray[$name]);
	}
	
	// ��������� ��������
	public function SaveProperty($name,$vl) {
		$this->CurrentSettingsArray[$this->currentCategory][$name] = $vl;
	}
	
	// ��������� ��������� ��������
	public function SaveSingleProperty($cat,$name,$vl,$create_if = false) {
		if (strval($cat) && strval($name)) {
			if ($this->IsExistsProperty($name,$cat)) {
				$this->CurrentSettingsArray[$cat][$name] = $vl;
			}else{
				if (!$create_if) {
					$this->LAST_ERROR = '���������� �� ��������. �������� "'.$cat.'"�� ����������.';
				}else{
					$this->CurrentSettingsArray[$cat][$name] = $vl;
				}
			}
		}else{
			$this->LAST_ERROR = '���������� �� ��������. �������� �������� �������� � �������� �� ����� ���� �������';
		}
	}
	
	// ������ ��������
	public function ReadProperty($name) {
		if ($this->IsExistsProperty($name)) {
			return $this->CurrentSettingsArray[$this->currentCategory][$name];
		}else{
			$this->LAST_ERROR = '������� �� ��������. �������� "'.$name.'" �� ����������.';
		}
	}
	
	// ������ ��������� ��������
	public function ReadSingleProperty($cat,$name) {
		if (strval($cat) && strval($name)) {
			if ($this->IsExistsProperty($name,$cat)) {
				return $this->CurrentSettingsArray[$cat][$name];
			}else{
				$this->LAST_ERROR = '������� �� ��������. �������� "'.$name.'" �� ����������.';
			}
		}else{
			$this->LAST_ERROR = '������ �� ��������. �������� �������� �������� � �������� �� ����� ���� �������';
		}
	}
	
}

?>