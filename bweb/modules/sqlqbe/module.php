<?

/* *******************************************************************************

	static class: sqlQBE (query by example - sql)
	author: Viktor Kulikov aka BlackWeb (bezdoom@gmail.com)
	
	ver. 1.01
	14 april 2011
	
	������� ������������: 1c-bitrix.ru :)
	
	http://www.taldom.org

******************************************************************************** */

class sqlQBE {
	
	// ���������� ������� � ������� ������ �� ���������� ����������
	static private function litByKey($key,$type = 1) {
		
		$CNT = 0;
		if ($type == 1 && $key!=false) {
			$variable = array(
				'!=', '>=', '<=', '=', '>', '<', '!'
			);
			
			foreach ($variable as $k => $value) {
				if (str_replace($value,'',$key) != $key) {
					$CNT++;
					$arr['key'] = str_ireplace($variable,'',$key);
					$arr['lit'] = ($value == '!') ? '!=' : $value;
					break;
				}
			}
			
			if ($CNT == 0) {
				$arr['key'] = $key;
				$arr['lit'] = '=';
			}
			
			return $arr;
					
		}elseif ($type == 2 && $key!=false) {
			$variable = array(
				"||", "&&"
			);
			
			foreach ($variable as $k => $value) {
				if (str_replace($value,'',$key) != $key) {
					$CNT++;
					$arr['key'] = str_ireplace($variable,'',$key);
					$arr['lit'] = ($value=='||') ? 'OR' : 'AND';
					break;
				}
			}
			
			if ($CNT == 0) {
				$arr['key'] = $key;
				$arr['lit'] = 'AND';
			}
			
			return $arr;
			
		}
		
	}
	
	// �������������� ������ ���� "SELECT"
	static function select($table_name, $Q) {
		
		$as_arr = array();
		
		// ����������� � ���������� ������ ������
		$TABLE_CNT = 0;
		$originalTableArray = $table_name;
		if (!is_array($table_name)) {
			$table_name = "`".$table_name."`";
		}else{
			foreach($table_name as $keyTable => $nameTable) {
				$TABLE_CNT++;
				$table_name_new .= (($TABLE_CNT > 1) ? "," : "")."`".$nameTable."`";
			}
			
			$table_name = $table_name_new; 
		}
		
		$SELECT_SQL = "";
		if (isset($Q['SELECT']) && $Q['SELECT']!=false && !empty($Q['SELECT'])) {
			// ������ ���� �������� 
			$SELECT_CHT = 0;
			if (count($originalTableArray)>1) {
				
				foreach ($Q['SELECT'] as $k => $v) {
					if (is_array($v) && (intval($k) || $k == 0)) {
						foreach ($v as $key => $value) {
							$SELECT_CHT++;
							
							if (intval($key)) {
								if (!is_array($value)) {
									$SELECT_SQL .= (($SELECT_CHT > 1) ? "," : "")."`".$originalTableArray[$k]."`.`".$value."`";
								}else{
									foreach($value as $name => $likethis) {
										$SELECT_SQL .= (($SELECT_CHT > 1) ? "," : "")."`".$originalTableArray[$k]."`.`".$name."` as `".$likethis."`";
										$as_arr[] = $likethis;
									}
								}
							}elseif (strval($key)) {
								if (!is_array($value)) {
									$SELECT_SQL .= (($SELECT_CHT > 1) ? "," : "").$key."(`".$originalTableArray[$k]."`.`".$value."`)";
								}else{
									foreach($value as $name => $likethis) {
										$SELECT_SQL .= (($SELECT_CHT > 1) ? "," : "").$key."(`".$originalTableArray[$k]."`.`".$name."`) as `".$likethis."`";
										$as_arr[] = $likethis;
									}
								}
							}else{
								if (!is_array($value)) {
									$SELECT_SQL .= (($SELECT_CHT > 1) ? "," : "")."`".$originalTableArray[$k]."`.`".$value."`";
								}else{
									foreach($value as $name => $likethis) {
										$SELECT_SQL .= (($SELECT_CHT > 1) ? "," : "")."`".$originalTableArray[$k]."`.`".$name."` as `".$likethis."`";
										$as_arr[] = $likethis;
									}
								}
							}
						}
					}
				}				
				
			}else{
				
				foreach ($Q['SELECT'] as $key => $value) {
					$SELECT_CHT++;
					
					if (intval($key)) {
						if (!is_array($value)) {
							$SELECT_SQL .= (($SELECT_CHT > 1) ? "," : "")."`".$value."`";
						}else{
							foreach($value as $name => $likethis) {
								$SELECT_SQL .= (($SELECT_CHT > 1) ? "," : "")."`".$name."` as `".$likethis."`";
								$as_arr[] = $likethis;
							}
						}
					}elseif (strval($key)) {
						if (!is_array($value)) {
							$SELECT_SQL .= (($SELECT_CHT > 1) ? "," : "").$key."(`".$value."`)";
						}else{
							foreach($value as $name => $likethis) {
								$SELECT_SQL .= (($SELECT_CHT > 1) ? "," : "").$key."(`".$name."`) as `".$likethis."`";
								$as_arr[] = $likethis;
							}
						}
					}else{
						if (!is_array($value)) {
							$SELECT_SQL .= (($SELECT_CHT > 1) ? "," : "")."`".$value."`";
						}else{
							foreach($value as $name => $likethis) {
								$SELECT_SQL .= (($SELECT_CHT > 1) ? "," : "")."`".$name."` as `".$likethis."`";
								$as_arr[] = $likethis;
							}
						}
					}
				}
				
			}
			// ����������� ���� �������
			$SELECT_SQL = "SELECT ".$SELECT_SQL." FROM ".$table_name;
		}else{
			$SELECT_SQL = "SELECT * FROM ".$table_name;
		}
		
		$JOIN_SQL = "";
		if (isset($Q['INNERJOIN']) && $Q['INNERJOIN']!=false && !empty($Q['INNERJOIN']) && !isset($Q['OUTERJOIN']) 
			&& is_array($originalTableArray) && count($Q['INNERJOIN'])==1 && count($originalTableArray)>1) {
			// ������ ���� ������� 
			foreach ($Q['INNERJOIN'] as $key => $value) {				
				if (is_array($value) && (intval($key) || $key == 0)) {
					// ���� ����� ���� inner
					if (!empty($originalTableArray[$key])) {
						foreach ($value as $k => $v) {
							if (is_array($v)) {
								foreach ($v as $tableId => $fieldtitle) {
									$JOIN_SQL = "`".$originalTableArray[$key]."` ON `".$originalTableArray[$tableId]."`.`".$fieldtitle."`=`".$originalTableArray[$key]."`.`".$k."`";
									break;
								}
							}
							break;
						}
					}
					
				}	
				break;			
			}
			
			$JOIN_SQL = " INNER JOIN ".$JOIN_SQL;
		}
		
		$WHERE_SQL = "";
		if (isset($Q['WHERE']) && $Q['WHERE']!=false && !empty($Q['WHERE'])) {
			// ������ ���� ������� 
			$WHERE_CHT = 0;
			if (count($originalTableArray)>1) {
				
				foreach ($Q['WHERE'] as $tabid => $val) {
					if (is_array($val) && (intval($tabid) || $tabid == 0)) {
						foreach ($val as $key => $value) {
							if (!intval($key) && ($key!=false)) {
								$WHERE_CHT++;
								if (!is_array($value)) {
									$arkey = sqlQBE::litByKey($key);
									$WHERE_SQL .= (($WHERE_CHT > 1) ? " AND " : " ")."(`".$originalTableArray[$tabid]."`.`".$arkey['key']."`".$arkey['lit']."'".$value."')";
								}else{
									$arkey = sqlQBE::litByKey($key);
									$INARR_SQL = ''; $INARR_CNT = 0;
									foreach($value as $k => $v) {
										if (!is_array($v)) {
											$INARR_CNT++;
											$arrvl = sqlQBE::litByKey($v,2);
											if ($INARR_CNT == 1) {
												$firstPref = $arrvl['lit'];
											}
											$INARR_SQL .= (($INARR_CNT > 1) ? " ".$arrvl['lit']." " : " ")."(`".$originalTableArray[$tabid]."`.`".$arkey['key']."`".$arkey['lit']."'".$arrvl['key']."')";
										}
									}
									
									$WHERE_SQL .= (($WHERE_CHT > 1) ? " ".$firstPref." " : " ")."(".$INARR_SQL.")";
								}
							}
						}						
					}
				}
				
			}else{
				foreach ($Q['WHERE'] as $key => $value) {
					if (!intval($key) && ($key!=false)) {
						$WHERE_CHT++;
						if (!is_array($value)) {
							$arkey = sqlQBE::litByKey($key);
							$WHERE_SQL .= (($WHERE_CHT > 1) ? " AND " : " ")."(`".$arkey['key']."`".$arkey['lit']."'".$value."')";
						}else{
							$arkey = sqlQBE::litByKey($key);
							$INARR_SQL = ''; $INARR_CNT = 0;
							foreach($value as $k => $v) {
								if (!is_array($v)) {
									$INARR_CNT++;
									$arrvl = sqlQBE::litByKey($v,2);
									if ($INARR_CNT == 1) {
										$firstPref = $arrvl['lit'];
									}
									$INARR_SQL .= (($INARR_CNT > 1) ? " ".$arrvl['lit']." " : " ")."(`".$arkey['key']."`".$arkey['lit']."'".$arrvl['key']."')";
								}
							}
							
							$WHERE_SQL .= (($WHERE_CHT > 1) ? " ".$firstPref." " : " ")."(".$INARR_SQL.")";
						}
					}
				}
			}
			
			$WHERE_SQL = " WHERE ".$WHERE_SQL;
		}
		
		$GROUP_SQL = "";
		if (isset($Q['GROUP']) && $Q['GROUP']!=false && !empty($Q['GROUP']) && (count($Q['GROUP'])==1)) {
			// ������ ���� ����������� 
			if (count($originalTableArray)>1) {
				foreach ($Q['GROUP'] as $tabid => $val) {
					if (is_array($val) && (strtoupper($tabid) == "AS")) {
						print "as";
						foreach ($val as $key => $value) {
							if (strval($value) && $value!=0)
							$GROUP_SQL .= "`".$value."`";
							else
							$GROUP_SQL .= "`".$as_arr[$value]."`";
						}
					}elseif (is_array($val) && (intval($tabid) || $tabid == 0)) {
						foreach ($val as $key => $value) {
							$GROUP_SQL .= "`".$originalTableArray[$tabid]."`.`".$value."`";
						}
					}
				}
			}else{
				$GROUP_SQL .= "`".$Q['GROUP'][0]."`";
			}
			$GROUP_SQL = " GROUP BY ".$GROUP_SQL;
		}
		
		$ORDER_SQL = "";
		if (isset($Q['ORDER']) && $Q['ORDER']!=false && !empty($Q['ORDER'])) {
			// ������ ���� ���������� 
			$ORDER_CHT = 0;
			if (count($originalTableArray)>1) {
				foreach ($Q['ORDER'] as $tabid => $val) {
					if (is_array($val) && (intval($tabid) || $tabid == 0)) {
						
						foreach ($val as $key => $value) {
							if (intval($key) || $key==false) {
								if (strtoupper($value) == 'RAND'){
									$ORDER_SQL = 'RAND()';
									break;
								}else{
									$ORDER_CHT++; 
									$ORDER_SQL .= (($ORDER_CHT > 1) ? "," : "")."`".$originalTableArray[$tabid]."`.`".$value."`";
								}
							}else{
								$ORDER_CHT++;
								if ((strtoupper($value) == 'ASC') || (strtoupper($value) == 'DESC')) {
									$ORDER_SQL .= (($ORDER_CHT > 1) ? "," : "")."`".$originalTableArray[$tabid]."`.`".$key."` ".strtoupper($value);
								}else{
									$ORDER_SQL .= (($ORDER_CHT > 1) ? "," : "")."`".$originalTableArray[$tabid]."`.`".$key."`";	
								}					
							}
						}
						
					}
				}
			}else{
				foreach ($Q['ORDER'] as $key => $value) {
					if (intval($key) || $key==false) {
						if (strtoupper($value) == 'RAND'){
							$ORDER_SQL = 'RAND()';
							break;
						}else{
							$ORDER_CHT++; 
							$ORDER_SQL .= (($ORDER_CHT > 1) ? "," : "")."`".$value."`";
						}
					}else{
						$ORDER_CHT++;
						if ((strtoupper($value) == 'ASC') || (strtoupper($value) == 'DESC')) {
							$ORDER_SQL .= (($ORDER_CHT > 1) ? "," : "")."`".$key."` ".strtoupper($value);
						}else{
							$ORDER_SQL .= (($ORDER_CHT > 1) ? "," : "")."`".$key."`";	
						}					
					}
				}
			}
			
			$ORDER_SQL = " ORDER BY ".$ORDER_SQL;
		}
		
		$LIMIT_SQL = "";
		if (isset($Q['LIMIT']) && $Q['LIMIT']!=false && !empty($Q['LIMIT']) && (count($Q['LIMIT'])==1)) {
			// ������ ���� ��������  ((( �������� 4 ������� � 5 (4=>5) )))
			$LIMIT_CHT = 0;
			if (intval($Q['LIMIT'][0]) && $Q['LIMIT'][0]>0) {
				$LIMIT_SQL = $Q['LIMIT'][0];
			}else{
				foreach ($Q['LIMIT'] as $key => $value) {
					$LIMIT_SQL = $value.','.$key;
				}
			}
			
			$LIMIT_SQL = " LIMIT ".$LIMIT_SQL;
		}
		
		// ������� ����������� ������
		$COMPILED_SQL = $SELECT_SQL.$JOIN_SQL.$WHERE_SQL.$GROUP_SQL.$ORDER_SQL.$LIMIT_SQL;
		
		return $COMPILED_SQL;
		
	}
	
	/////////////////////////
	//// �������� ������ ////
	/////////////////////////
	
	// ������ ��� ������� ��� ��� �����
	static public function pSelect($arSelect) {
		
		$SQL_SELECT = '';
		$SQL_CNT = 0;
		
		if (is_array($arSelect) && !empty($arSelect)) {
			// ������ arSelect
			foreach ($arSelect as $key => $value) {
				$SQL_CNT++;
				$SQL_SELECT .= (($SQL_CNT > 1) ? "," : "")."'".$value."'";										
			}
			
		}else $SQL_SELECT = '*';
		
		return $SQL_SELECT;
		
	}
	
	function pWhere($arWhere) {
		
		$WHERE_SQL = "";
		$WHERE_CHT = 0;
		
		if(is_array($arWhere) && !empty($arWhere)) {
			foreach ($arWhere as $key => $value) {
				if (!intval($key) && ($key!=false)) {
					$WHERE_CHT++;
					if (!is_array($value)) {
						$arkey = self::litByKey($key);
						$WHERE_SQL .= (($WHERE_CHT > 1) ? " AND " : " ")."(`".$arkey['key']."`".$arkey['lit']."'".$value."')";
					}else{
						$arkey = self::litByKey($key);
						$INARR_SQL = ''; $INARR_CNT = 0;
						foreach($value as $k => $v) {
							if (!is_array($v)) {
								$INARR_CNT++;
								$arrvl = self::litByKey($v,2);
								if ($INARR_CNT == 1) {
									$firstPref = $arrvl['lit'];
								}
								$INARR_SQL .= (($INARR_CNT > 1) ? " ".$arrvl['lit']." " : " ")."(`".$arkey['key']."`".$arkey['lit']."'".$arrvl['key']."')";
							}
						}
						
						$WHERE_SQL .= (($WHERE_CHT > 1) ? " ".$firstPref." " : " ")."(".$INARR_SQL.")";
					}
				}
			}
			
			$WHERE_SQL = " WHERE ".$WHERE_SQL;
		}else $WHERE_SQL = "";
		
		return $WHERE_SQL;
		
	}
	
}

?>