<?

// ����� ���� ������
error_reporting(E_ALL);

// �������� �������������� ��������� php.ini 
require_once ('php.ini.php');

// ��������� �������
define('KERNEL_VER', '1.2.1');
define('PROJECT_TITLE', '������ ����� ���');
define('DOMAIN', 'newtaldom.org');

// �������������� ���������
define('IF_EMPTY_TITLE', '��� ���������');

// ������ ��������� �� ��������� �� ������ ��� ���������:)
define('DS', DIRECTORY_SEPARATOR);
// ��������� ������� ����� � �����
define('BWEB_FOLDER', 'bweb');
// �������� ������ ���� �� �������� ����� �� �������
define('ROOTDIR', str_ireplace(DS . BWEB_FOLDER . DS . 'system.php','',__FILE__));
// ��������� �������� ����� ��� ������� ������ ������
define('KERNEL_FOLDER_NAME', 'kernel' . DS);
// �������� ���� �� ��� ��-���������
define('CACHE_ROOT', 'cache'. DS);
// ���������� � ��������
define('MODULES_ROOT', BWEB_FOLDER . DS . 'modules' . DS);
// ���������� ��� �������� ������ �� ���������
define('UPLOAD_ROOT', 'uploads' . DS);
// �������� ����������
define('LANGUAGE_ROOT', BWEB_FOLDER . DS . 'languages');

// �������������� ������� 
unset($GLOBALS['NATIVE']['LANGUAGE']);

// �������� ������
session_start();

// ������� ���������� common �� ���������������� ��������� � ������ �������
require_once ('defines.php');
require_once ('common.php');
require_once ('functions.php');

// ���������� ������ ��� ������ � ��������������
require_once (KERNEL_FOLDER_NAME . "user.sys.php");
// ���������� ������ ��� ������ � �������� ������������
require_once (KERNEL_FOLDER_NAME . "cache.files.php");
// ���������� ������ ��� ������ � memcached
require_once (KERNEL_FOLDER_NAME . "cache.memcached.php");
// ���������� ������������
require_once (KERNEL_FOLDER_NAME . "view.native.php");
// ���������� ������������ ��������
require_once (KERNEL_FOLDER_NAME . "components.sys.php");

// �������� ������
if (!defined('DISABLE_MYSQL')) {
	env::mysql();
	if (!defined('MYSQL_ENGINE') || MYSQL_ENGINE == 'mysql') {
		mysql_set_charset(MYSQL_CHARSET);
	}elseif(MYSQL_ENGINE == 'mysqli') {
		global $dblink;
		mysqli_set_charset($dblink, MYSQL_CHARSET);
	} 
}

// ����������� � �������
if (!defined('DISABLE_AUTH')) {
	require_once ('auth.php');
}

?>